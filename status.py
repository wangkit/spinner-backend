class Status:
    no_profile = 199
    ok = 200
    has_profile = 201
    has_preference = 202
    already_passed = 205
    already_liked = 206
    already_greeted = 210
    exceed_discover_allow = 211
    otp_failed = 403
    exception_happened = 405
    no_preference = 408
    wrong_token = 409
    too_many_emails = 410
    too_many_data = 411
    is_nude = 412
    duplicate_email = 444
    client_already_exists = 500
    duplicate_fcm_token = 501
    expired_otp = 510
    blacklisted = 555
    already_verified = 591
    no_such_account = 592


class TokenStatus:
    ok = 0
    wrong = 1
    expired = 2


class UserStatus:
    normal = 0
    deleted = 1
    banned = 2


class GenderStatus:
    male = 0
    female = 1
    transgender = 2
    gender_queer = 3
    agender = 4


class PreferredRelationshipStatus:
    marriage = 0
    longTerm = 1
    causal = 2
    noPreference = 3
