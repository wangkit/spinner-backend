class ChatTable: 
    tableName = 'chat'
    sender_id = 'senderId'
    updated_at = 'updatedAt'
    target_email = 'targetEmail'
    last_message = 'lastMessage'
    target_color = 'targetColor'
    target_id = 'targetId'
    target_last_name = 'targetLastName'
    target_first_name = 'targetFirstName'
    created_at = 'createdAt'
    is_last_message_myself = 'isLastMessageMyself'
    has_unread = 'hasUnread'
    target_images = 'targetImages'
