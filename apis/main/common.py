from random import randint
from flask import jsonify
from sql import get_profile_body, get_preference_body, get_to_char_timestamp
from values.activity import *
from values.log import *
from status import Status, UserStatus
from utility.utils import *
from constant.const import *
from values.data_key import DataKey
from values.action import Action
from . import main
import firebase_admin
from firebase_admin import credentials, firestore
from values.tables import *
from values.push_messages import *


cred = credentials.Certificate("spinner.json")
firebase_admin.initialize_app(cred)
firestore_db = firestore.client()


@main.route('{endpoint}/editProfile'.format(endpoint=endpoint), methods=['POST'])
def edit_profile():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        email = request.form[DataKey.email]
        id = request.form[DataKey.id]
        if is_valid_token(token):
            first_name = request.form[DataKey.first_name]
            last_name = request.form[DataKey.last_name]
            country = request.form[DataKey.country]
            city = request.form[DataKey.city]
            longitude = float(request.form[DataKey.longitude])
            # noinspection PyPackageRequirements
            latitude = float(request.form[DataKey.latitude])
            gender = int(request.form[DataKey.gender])
            color = request.form[DataKey.color]
            birthday = request.form[DataKey.birthday]
            age = int(request.form[DataKey.age])
            ethnicity = request.form[DataKey.ethnicity]
            religion = request.form[DataKey.religion]
            zodiac_sign = request.form[DataKey.zodiac_sign]
            profession = request.form[DataKey.profession]
            education = request.form[DataKey.education]
            graduated_from = request.form[DataKey.graduated_from]
            personality = request.form[DataKey.personality]
            verification_instruction = request.form[DataKey.instruction]
            preferred_relationship = int(request.form[DataKey.preferred_relationship])
            is_green_head = request.form[DataKey.is_green_head] == true_text
            hobby = request.form[DataKey.hobby]
            if len(city) == 0 or city == null_text:
                city = get_city_name(get_ip())
            if len(country) == 0 or country == null_text:
                country = get_country_name(get_ip())
            cur.execute("select id from profile where id = %s", (id,))
            fetched_data = fetch_data_in_list()
            if len(fetched_data) == 0:
                return jsonify({DataKey.status: Status.no_such_account})
            else:
                cur.execute("""
update profile set
first_name = %s,
last_name = %s, 
country = %s,
city = %s,
longitude = %s,
latitude = %s,
gender = %s,
color = %s,
birthday = %s,
age = %s,
ethnicity = %s,
religion = %s, 
zodiac_sign = %s,
profession = %s,
education = %s,
graduated_from = %s,
personality = %s,
preferred_relationship = %s,
is_green_head = %s,
hobby = %s,
verification_instruction = %s,
last_active = {time},
updated_at = {time}
where id = %s
""".format(time=get_time_now()), (
                first_name, last_name, country, city, longitude, latitude, gender, color,
                birthday, age, ethnicity, religion, zodiac_sign, profession, education, graduated_from,
                personality, preferred_relationship, is_green_head, hobby, verification_instruction, id))
                conn.commit()
                cur.execute("update preference set preferred_relationship = (select preferred_relationship from preference where id = %s) where id = %s", (id, id,))
                conn.commit()
                insert_log(uniqueid, edit_profile_log, id)
                return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/createProfile'.format(endpoint=endpoint), methods=['POST'])
def create_profile():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = str(uuid.uuid4())
        if is_valid_token(token):
            first_name = request.form[DataKey.first_name]
            last_name = request.form[DataKey.last_name]
            password = request.form[DataKey.password]
            email = request.form[DataKey.email]
            country = request.form[DataKey.country]
            city = request.form[DataKey.city]
            longitude = float(request.form[DataKey.longitude])
            latitude = float(request.form[DataKey.latitude])
            gender = int(request.form[DataKey.gender])
            color = request.form[DataKey.color]
            birthday = request.form[DataKey.birthday]
            age = int(request.form[DataKey.age])
            ethnicity = request.form[DataKey.ethnicity]
            religion = request.form[DataKey.religion]
            zodiac_sign = request.form[DataKey.zodiac_sign]
            profession = request.form[DataKey.profession]
            education = request.form[DataKey.education]
            graduated_from = request.form[DataKey.graduated_from]
            personality = request.form[DataKey.personality]
            verification_instruction = request.form[DataKey.instruction]
            preferred_relationship = int(request.form[DataKey.preferred_relationship])
            is_green_head = request.form[DataKey.is_green_head] == true_text
            hobby = request.form[DataKey.hobby]
            if len(city) == 0 or city == null_text:
                city = get_city_name(get_ip())
            if len(country) == 0 or country == null_text:
                country = get_country_name(get_ip())
            cur.execute("select email from profile where email = %s", (email,))
            fetched_email = fetch_data_in_list()
            if len(fetched_email) > 0:
                return jsonify({DataKey.status: Status.duplicate_email})
            else:
                print("NEW_SPINNER_PROFILE")
                cur.execute("""
                insert into profile (uniqueid, id, first_name, last_name, email, country, city, longitude, latitude, gender, color, birthday, age, ethnicity, religion, zodiac_sign, profession, education, graduated_from, personality, preferred_relationship, is_green_head, hobby, verification_instruction, last_active, updated_at, created_at, password, images, video, video_thumbnail, verification_image)
                values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, {time}, {time}, {time}, %s, %s, %s, %s, %s)
                       """.format(time=get_time_now()), (
                uniqueid, id, first_name, last_name, email, country, city, longitude, latitude, gender, color,
                birthday, age, ethnicity, religion, zodiac_sign, profession, education, graduated_from,
                personality, preferred_relationship, is_green_head, hobby, verification_instruction, password, [placeholder_link, placeholder_link, placeholder_link, placeholder_link, placeholder_link, placeholder_link], "", "", placeholder_link,))
                conn.commit()
                insert_log(uniqueid, create_profile_log, id)
                return jsonify({DataKey.status: Status.ok, DataKey.id: id})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/editStrictPreference'.format(endpoint=endpoint), methods=['POST'])
def edit_strict_preference():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        if is_valid_token(token):
            distance = request.form[DataKey.distance] == true_text
            age = request.form[DataKey.age] == true_text
            like_count = request.form[DataKey.like_count] == true_text
            gender = request.form[DataKey.gender] == true_text
            ethnicity = request.form[DataKey.ethnicity] == true_text
            religion = request.form[DataKey.religion] == true_text
            zodiac_sign = request.form[DataKey.zodiac_sign] == true_text
            education = request.form[DataKey.education] == true_text
            hobby = request.form[DataKey.hobby] == true_text
            is_green_head = request.form[DataKey.is_green_head] == true_text
            preferred_relationship = request.form[DataKey.preferred_relationship] == true_text
            cur.execute("""
update strict_preference set gender = %s, distance = %s, age = %s, ethnicity = %s, 
religion = %s, zodiac_sign = %s, education = %s, hobby = %s, is_green_head = %s, 
preferred_relationship = %s, liked_count = %s, updated_at = {time} where id = %s
            """.format(time=get_time_now()), (gender, distance, age, ethnicity, religion, zodiac_sign, education, hobby, is_green_head, preferred_relationship, like_count, id,))
            conn.commit()
            insert_log(uniqueid, edit_strict_preference_log, id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/createUpdatePreference'.format(endpoint=endpoint), methods=['POST'])
def create_update_preference():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        email = request.form[DataKey.email]
        if is_valid_token(token):
            # Get id and uniqueid of that email
            cur.execute("select id, uniqueid from preference where email = %s or id = %s", (email, id,))
            data = fetch_data_in_list()
            if len(data) > 0:
                is_create = False
                # if the received id is the same as the assigned id of the email, this request is valid
                if data[0] != id:
                    insert_log(uniqueid, wrong_id_log, id)
                    return jsonify({DataKey.status: Status.exception_happened})
            else:
                is_create = True
            distance = int(request.form[DataKey.distance])
            age = request.form[DataKey.age]
            like_count = request.form[DataKey.like_count]
            gender = request.form[DataKey.gender]
            ethnicity = request.form[DataKey.ethnicity]
            religion = request.form[DataKey.religion]
            zodiac_sign = request.form[DataKey.zodiac_sign]
            education = request.form[DataKey.education]
            hobby = request.form[DataKey.hobby]
            is_green_head = request.form[DataKey.is_green_head]
            preferred_relationship = int(request.form[DataKey.preferred_relationship])
            hobby = convert_str_to_list(hobby)
            education = convert_str_to_list(education)
            zodiac_sign = convert_str_to_list(zodiac_sign)
            religion = convert_str_to_list(religion)
            ethnicity = convert_str_to_list(ethnicity)
            gender = convert_str_to_list_of_int(gender)
            age = convert_str_to_list_of_int(age)
            like_count = convert_str_to_list_of_int(like_count)
            if is_green_head == null_text:
                is_green_head = None
            else:
                is_green_head = is_green_head == true_text
            if is_create:
                cur.execute("""
    insert into preference (uniqueid, id, email, gender, distance, age, ethnicity, religion, zodiac_sign, education, hobby, is_green_head, preferred_relationship, updated_at, created_at, liked_count)
    values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, {time}, {time}, %s)
                """.format(time=get_time_now()), (uniqueid, id, email, gender, distance, age, ethnicity, religion, zodiac_sign, education, hobby, is_green_head, preferred_relationship, like_count,))
                # has to commit above sql before executing below
                conn.commit()
                cur.execute(
                    "update preference set preferred_relationship = (select preferred_relationship from profile where id = %s and email = %s) where id = %s and email = %s",
                    (id, email, id, email,))
                conn.commit()
                cur.execute("insert into strict_preference (id, updated_at, created_at) values (%s, {time}, {time})".format(time=get_time_now()), (id,))
                conn.commit()
                insert_log(uniqueid, create_preference_log, id)
            else:
                if data[1] != uniqueid:
                    # update the uniqueid if it is different from what we received
                    # This is only needed in updating preference
                    cur.execute("update preference set uniqueid = %s where id = %s", (uniqueid, id,))
                    cur.execute("update profile set uniqueid = %s where id = %s", (uniqueid, id,))
                    conn.commit()
                    insert_log(uniqueid, new_phone_log + str(data[1]), id)
                cur.execute("""
update preference set gender = %s, distance = %s, age = %s, ethnicity = %s, 
religion = %s, zodiac_sign = %s, education = %s, hobby = %s, is_green_head = %s, 
preferred_relationship = %s, liked_count = %s, updated_at = {time} where id = %s and email = %s
                """.format(time=get_time_now()), (gender, distance, age, ethnicity, religion, zodiac_sign, education, hobby, is_green_head, preferred_relationship, like_count, id, email))
                # has to commit above sql before executing below
                conn.commit()
                cur.execute(
                    "update profile set preferred_relationship = (select preferred_relationship from preference where id = %s and email = %s) where id = %s and email = %s",
                    (id, email, id, email,))
                conn.commit()
                insert_log(uniqueid, edit_preference_log, id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/checkOneTimePassword'.format(endpoint=endpoint), methods=['POST'])
def check_one_time_password():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        otp = request.form[DataKey.otp]
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        if is_valid_token(token):
            if otp == auto_otp and auto_pass == "ON":
                return jsonify({DataKey.status: Status.ok})
            saved_otp = r.get(uniqueid + otp_key)
            saved_otp = convert_byte_to_str(saved_otp)
            saved_time = r.get(uniqueid + otp_time)
            now = datetime.datetime.utcnow()
            saved_time = datetime.datetime.strptime(convert_byte_to_str(saved_time), datetime_format)
            if otp == saved_otp:
                if now > saved_time:
                    insert_log(uniqueid, otp_expire_log)
                    return jsonify({DataKey.status: Status.expired_otp})
                else:
                    r.delete(uniqueid + otp_key)
                    r.delete(uniqueid + otp_time)
                    insert_log(uniqueid, otp_success_log)
                    return jsonify({DataKey.status: Status.ok})
            else:
                insert_log(uniqueid, otp_wrong_log)
                return jsonify({DataKey.status: Status.otp_failed})
        else:
            insert_log(uniqueid, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error while checking otp")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/sendVerificationCode'.format(endpoint=endpoint), methods=['POST'])
def send_verification_code():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        email = request.form[DataKey.email]
        token = request.form[DataKey.token]
        device_name = request.form[DataKey.device_name]
        uniqueid = request.form[DataKey.uniqueid]
        if is_valid_token(token):
            now = datetime.datetime.utcnow()
            last_time = r.get(uniqueid + email_time)
            last_time = convert_byte_to_str(last_time)
            # Has - and : mean this is a valid time
            if "-" in last_time and ":" in last_time:
                last_time = datetime.datetime.strptime(last_time, datetime_format)
                last_time_plus_fifty_nine_seconds = last_time + datetime.timedelta(seconds=59)
                if now < last_time_plus_fifty_nine_seconds:
                    sec_diff = (last_time_plus_fifty_nine_seconds - now).total_seconds()
                    insert_log(uniqueid, too_many_otp_email_log)
                    return jsonify({DataKey.status: Status.too_many_emails, DataKey.secDiff: sec_diff})
            r.delete(uniqueid + otp_key)
            r.delete(uniqueid + otp_time)
            otp = str(randint(100000, 999999))
            print("otp: " + otp)
            r.set(uniqueid + otp_key, otp)
            r.set(uniqueid + otp_time, (datetime.datetime.utcnow() + datetime.timedelta(minutes=otp_expire_time)).strftime(datetime_format))
            r.set(uniqueid + email_time, datetime.datetime.utcnow().strftime(datetime_format))
            from_email = support_email
            subject = '[Spinner] One-time Password'
            content = """
Dear User,

This email address has been used to register a Spinner account. To complete the registration, please enter the one-time password.

Device: {device_name}
One-time password: {otp}

Please note that this password will expire in 30 minutes.

If you have never attempted to create a Spinner account, please reply to admin@yarner.app. We will remove any personal information related to this email address from our storage as soon as possible.

Thanks,
Spinner of Yarns
            """.format(otp=otp, device_name=device_name)
            send_email(from_email, email, subject, content)
            insert_log(uniqueid, get_otp_log)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error while sending verification code")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/createClient'.format(endpoint=endpoint), methods=['POST'])
def insert_client():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        uniqueid = str(uuid.uuid4())
        email = ""
        device_name = request.form[DataKey.device_name]
        device_version = request.form[DataKey.device_version]
        device_id = request.form[DataKey.device_id]
        cur.execute("select uniqueid from client where device_id = %s", (device_id,))
        data = fetch_data_in_list()
        if len(data) > 0:
            uniqueid = data[0]
            cur.execute("select email from profile where uniqueid = %s and status = %s", (uniqueid, UserStatus.normal))
            email = fetch_data_in_list()
            if len(email) > 0:
                email = email[0]
                insert_log(uniqueid, old_user_back_log)
            else:
                email = ""
                insert_log(uniqueid, new_user_back_log)
        else:
            print("NEW_SPINNER_CLIENT")
            cur.execute(
                "insert into client (uniqueid, device_name, device_version, device_id, created_at) values (%s, %s, %s, %s, {time})".format(
                    time=get_time_now()), (uniqueid, device_name, device_version, device_id))
            conn.commit()
            insert_log(uniqueid, first_open_app_log)
        token = assign_new_token(uniqueid)
        return jsonify({DataKey.status: Status.ok, DataKey.uniqueid: uniqueid, DataKey.token: token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/flipIsPush'.format(endpoint=endpoint), methods=['POST'])
def flip_is_push():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        value = request.form[DataKey.value] == true_text
        if is_valid_token(token):
            cur.execute("update profile set is_push = %s where id = %s", (value, id,))
            conn.commit()
            insert_log(uniqueid, flip_is_push_log)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/updateVerificationImagePath'.format(endpoint=endpoint), methods=['POST'])
def update_verification_image_path():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        email = request.form[DataKey.email]
        image = request.form[DataKey.image]
        if is_valid_token(token):
            cur.execute("update profile set verification_image = %s where email = %s", (image, email,))
            conn.commit()
            insert_log(uniqueid, update_verification_image_log)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/updateImagePath'.format(endpoint=endpoint), methods=['POST'])
def update_images_path():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        email = request.form[DataKey.email]
        images_path = request.form[DataKey.image]
        is_verification = request.form[DataKey.is_verification] == true_text
        if not is_verification:
            images_path = convert_str_to_list(images_path)
        if is_valid_token(token):
            if is_verification:
                cur.execute("update profile set verification_image = %s where email = %s", (images_path, email,))
                conn.commit()
                insert_log(uniqueid, update_verification_image_log)
            else:
                for i in range(len(images_path)):
                    if images_path[i] != placeholder_link:
                        is_adult = check_is_adult(images_path[i], uniqueid)
                        print("is_adult: " + str(is_adult))
                        if is_adult:
                            print("images_path before: " + str(images_path))
                            images_path[i] = placeholder_link
                            print("images_path after: " + str(images_path))
                cur.execute("update profile set images = %s where email = %s", (images_path, email,))
                conn.commit()
                insert_log(uniqueid, update_images_log)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/block'.format(endpoint=endpoint), methods=['POST'])
def block_user():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        target_id = request.form[DataKey.target_id]
        reason = int(request.form[DataKey.reason])
        if is_valid_token(token):
            cur.execute("select * from blocked where id = %s and target_id = %s", (id, target_id,))
            data = fetch_data_in_list()
            if len(data) == 0:
                new_block_id = str(uuid.uuid4())
                cur.execute("delete from activity where (id = %s and target_id = %s) or (id = %s and target_id = %s)", (id, target_id, target_id, id,))
                conn.commit()
                cur.execute("insert into blocked (block_id, id, target_id, reason, created_at) values (%s, %s, %s, %s, {time})".format(time=get_time_now()), (new_block_id, id, target_id, reason,))
                conn.commit()
                insert_log(uniqueid, block_user_log + target_id, id=id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/updateVideoPath'.format(endpoint=endpoint), methods=['POST'])
def update_video_path():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        email = request.form[DataKey.email]
        video = request.form[DataKey.video]
        thumbnail = request.form[DataKey.thumbnail]
        if is_valid_token(token):
            cur.execute("update profile set video = %s, video_thumbnail = %s where email = %s", (video, thumbnail, email,))
            conn.commit()
            insert_log(uniqueid, update_video_log)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/checkEmail'.format(endpoint=endpoint), methods=['POST'])
def check_email():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        email = request.form[DataKey.email]
        if is_valid_token(token):
            cur.execute("select uniqueid from profile where email = %s", (email,))
            data = fetch_data_in_list()
            cur.execute("select id from preference where email = %s", (email,))
            pref = fetch_data_in_list()
            if len(data) > 0:
                has_profile = Status.has_profile
                if data[0] != uniqueid:
                    # Update ID
                    # This happens when the user has changed phone and is trying to login on a new phone
                    cur.execute("update profile set uniqueid = %s where email = %s", (uniqueid, email,))
                    cur.execute("update preference set uniqueid = %s where email = %s", (uniqueid, email,))
                    conn.commit()
                    insert_log(uniqueid, new_phone_log + str(data[0]))
            else:
                has_profile = Status.no_profile
            if len(pref) > 0:
                has_preference = Status.has_preference
            else:
                has_preference = Status.no_preference
            return jsonify({DataKey.status: Status.ok, DataKey.has_profile: has_profile, DataKey.has_preference: has_preference})
        else:
            insert_log(uniqueid, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/syncPrefRelationship'.format(endpoint=endpoint), methods=['POST'])
def sync_pref_relationship():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        email = request.form[DataKey.email]
        id = request.form[DataKey.id]
        if is_valid_token(token):
            cur.execute("update preference set preferred_relationship = (select preferred_relationship from profile where id = %s or email = %s)", (id, email,))
            conn.commit()
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/robotUser'.format(endpoint=endpoint), methods=['POST'])
def robot_user():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        target_id = request.form[DataKey.target_id]
        if is_valid_token(token):
            cur.execute("select id from robot_profile where id = %s and robot_id = %s union select id from verified_profile where id = %s and verified_id = %s", (id, target_id, id, target_id,))
            data = fetch_data_in_list()
            if len(data) > 0:
                return jsonify({DataKey.status: Status.already_verified})
            insert_into_activity(id, target_id, Activity.verify_as_robot)
            cur.execute("insert into robot_profile (id, robot_id, created_at) values (%s, %s, {time})".format(time=get_time_now()), (id, target_id,))
            conn.commit()
            insert_log(uniqueid, verify_user_log + target_id, id=id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id=id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/billing'.format(endpoint=endpoint), methods=['POST'])
def billing():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        product_id = request.form[DataKey.product_id]
        price = request.form[DataKey.price]
        currency_code = request.form[DataKey.currency_code]
        id = request.form[DataKey.id]
        if is_valid_token(token):
            cur.execute("insert into billing (id, product_id, price, currency_code, created_at) values (%s, %s, %s, %s, {time})".format(time=get_time_now()), (id, product_id, price, currency_code,))
            conn.commit()
            insert_log(uniqueid, buy_product_log + product_id, id=id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id=id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/setNoVip'.format(endpoint=endpoint), methods=['POST'])
def set_no_vip():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        if is_valid_token(token):
            cur.execute("update profile set is_vip = false where id = %s", (id,))
            conn.commit()
            insert_log(uniqueid, set_vip_log, id=id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id=id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/setVip'.format(endpoint=endpoint), methods=['POST'])
def set_vip():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        if is_valid_token(token):
            cur.execute("update profile set is_vip = true where id = %s", (id,))
            conn.commit()
            insert_log(uniqueid, set_vip_log, id=id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id=id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/actionLog'.format(endpoint=endpoint), methods=['POST'])
def action_log():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        action = int(request.form[DataKey.action])
        id = request.form[DataKey.id]
        if is_valid_token(token):
            insert_log(uniqueid, Action.map[action], id=id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id=id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/verifyUser'.format(endpoint=endpoint), methods=['POST'])
def verify_user():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        target_id = request.form[DataKey.target_id]
        if is_valid_token(token):
            cur.execute(
                "select id from robot_profile where id = %s and robot_id = %s union select id from verified_profile where id = %s and verified_id = %s",
                (id, target_id, id, target_id,))
            data = fetch_data_in_list()
            if len(data) > 0:
                return jsonify({DataKey.status: Status.already_verified})
            insert_into_activity(id, target_id, Activity.verify_as_user)
            cur.execute("insert into verified_profile (id, verified_id, created_at) values (%s, %s, {time})".format(time=get_time_now()), (id, target_id,))
            conn.commit()
            insert_log(uniqueid, verify_user_log + target_id, id=id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id=id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/sendGreeting'.format(endpoint=endpoint), methods=['POST'])
def send_greeting():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        message = request.form[DataKey.message]
        id = request.form[DataKey.id]
        target_id = request.form[DataKey.target_id]
        if is_valid_token(token):
            cur.execute("select * from greeting where id = %s and target_id = %s", (id, target_id,))
            data = fetch_data_in_list()
            if len(data) == 0:
                cur.execute("select first_name, last_name from profile where id = %s", (id,))
                names = fetch_data_in_list()
                if len(names) > 0:
                    first_name = names[0]
                    last_name = names[1]
                    push_message = first_name + " " + last_name + PushMessage.hello
                else:
                    first_name = "Someone"
                    last_name = ""
                    push_message = first_name + " " + PushMessage.hello
                greeting_and_activity_id = str(uuid.uuid4())
                cur.execute("update profile set last_active = {time} where id = %s".format(time=get_time_now()), (id,))
                cur.execute("insert into greeting (greeting_id, id, target_id, message, created_at) values (%s, %s, %s, %s, {time})".format(time=get_time_now()), (greeting_and_activity_id, id, target_id, message,))
                conn.commit()
                insert_into_activity(id=id, target_id=target_id, activity=Activity.greeting, activity_id=greeting_and_activity_id)
                insert_log(uniqueid, send_greeting_message_log, id=id)
                send_push(target_id, PushTitle.hello, push_message)
                return jsonify({DataKey.status: Status.ok})
            else:
                return jsonify({DataKey.status: Status.already_greeted})
        else:
            insert_log(uniqueid, wrong_token_log, id=id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/fetchActivity'.format(endpoint=endpoint), methods=['POST'])
def fetch_activity():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        start_from = int(request.form[DataKey.start_from])
        if is_valid_token(token):
            cur.execute("""
select 
activity_id,
id,
(select array[images[1], color, last_name, first_name] from profile where profile.id = activity.id),
target_id,
activity,
is_read,
{created_at},
(select message from greeting where greeting_id = activity_id)
from activity 
where target_id = %s 
order by created_at desc 
limit 50 offset {start_from}
            """.format(start_from=start_from, created_at=get_to_char_timestamp('created_at')), (id,))
            fetched_activity = fetch_data_in_list_of_list()
            insert_log(uniqueid, fetch_activity_log, id=id)
            return jsonify({DataKey.status: Status.ok, DataKey.activity: fetched_activity})
        else:
            insert_log(uniqueid, wrong_token_log, id=id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/getRandomDiscovers'.format(endpoint=endpoint), methods=['POST'])
def get_random_discovers():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        need_update_last_discover = False
        need_insert_last_discover = False
        last_discover_where_clause = ""
        last_data_where_clause = ""
        new_target_ids = []
        original_target_ids = []
        token = request.form[DataKey.token]
        is_refresh = request.form[DataKey.is_refresh] == true_text
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        main_sql = """
select {get_profile_body}
FROM profile where 
{profile_filter}
{last_discover}
order by random() desc limit {get_limit}
"""
        if is_valid_token(token):
            cur.execute("select target_ids from last_data where id = %s and created_at not between NOW() - INTERVAL '1 HOURS' AND NOW()", (id,))
            last_data = fetch_data_in_list()
            if len(last_data) > 0:
                last_data = last_data[0]
                last_data_where_clause = "and not (profile.id = any(%s))"
                last_data_where_clause = str(cur.mogrify(last_data_where_clause, (last_data,)))
                last_data_where_clause = last_data_where_clause.replace('b"', "").replace('"', "")
            cur.execute("select is_vip from profile where id = %s", (id,))
            is_vip = fetch_data_in_list()
            if len(is_vip) > 0:
                is_vip = is_vip[0]
            else:
                is_vip = False
            get_daily_refresh_count(id)
            today_refresh_count = fetch_data_in_list()
            if len(today_refresh_count) > 0:
                today_refresh_count = today_refresh_count[0]
            else:
                today_refresh_count = 0
            cur.execute("select target_ids, created_at from last_discover where id = %s", (id,))
            last_discover = fetch_data_in_list()
            if len(last_discover) > 0:
                last_created_at = str(last_discover[1])
                original_target_ids = last_discover[0]
                now = datetime.datetime.utcnow()
                last_created_at = datetime.datetime.strptime(last_created_at, datetime_format)
                next_fetch_time = last_created_at + datetime.timedelta(hours=get_discover_interval)
                if (is_refresh and is_vip and vip_refresh_daily_count >= today_refresh_count) or now > next_fetch_time:
                    if is_refresh and is_vip:
                        # Count refresh time
                        insert_refresh(id)
                    last_discover_where_clause = "and not (profile.id = any(%s))"
                    need_update_last_discover = True
                else:
                    insert_log(uniqueid, too_frequent_data_log)
                    last_discover_where_clause = "and profile.id = any(%s)"
            else:
                need_insert_last_discover = True
            main_sql = main_sql.format(
                id=id,
                get_profile_body=get_profile_body(id),
                last_discover=last_discover_where_clause,
                get_limit=get_limit(id),
                profile_filter=get_profile_filter(id),
                # last_data=last_data_where_clause,
            )
            if not need_insert_last_discover:
                cur.execute(main_sql, (get_profile_images_filter(), original_target_ids,))
            else:
                cur.execute(main_sql, (get_profile_images_filter(),))
            profiles = fetch_data_in_list_of_list()
            for profile in profiles:
                new_target_ids.append(profile[0])
            if need_update_last_discover:
                cur.execute("update last_discover set created_at = {time}, target_ids = %s where id = %s".format(time=get_time_now()), (new_target_ids, id,))
                conn.commit()
            if need_insert_last_discover:
                cur.execute("insert into last_discover (id, target_ids, created_at) values (%s, %s, {time})".format(time=get_time_now()), (id, new_target_ids,))
                conn.commit()
            cur.execute("""
select {get_preference_body}
from preference where id = any(%s)
            """.format(get_preference_body=get_preference_body()), (new_target_ids,))
            preferences = fetch_data_in_list_of_list()
            insert_log(uniqueid, get_discover_log, id)
            return jsonify({DataKey.status: Status.ok, DataKey.profiles: profiles, DataKey.preferences: preferences, DataKey.remaining_count: vip_refresh_daily_count - today_refresh_count - 1})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/getMyPotentialMatches'.format(endpoint=endpoint), methods=['POST'])
def get_my_potential_matches():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        need_update_last_data = False
        need_insert_last_data = False
        last_data_where_clause = ""
        new_target_ids = []
        original_target_ids = []
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        is_refresh = request.form[DataKey.is_refresh] == true_text
        id = request.form[DataKey.id]
        main_sql = """
select {get_profile_body}
from (
WITH pref AS (
SELECT * FROM preference WHERE id = '{id}'
),
my_profile as (
SELECT * from profile where id = '{id}'
),
strict as (
select * from strict_preference where id = '{id}'
)
SELECT 
profile.*,
(case when profile.gender = any(pref.gender) then 1 else 0 end + 
case when calculate_distance(profile.latitude, profile.longitude, my_profile.latitude, my_profile.longitude, 'K') <= pref.distance then 1 else 0 end + 
case when (SELECT date_part('year', age(profile.birthday::timestamp))::int) >= pref.age[1] and (SELECT date_part('year', age(profile.birthday::timestamp))::int) <= pref.age[2] then 1 else 0 end +
case when profile.ethnicity = any(pref.ethnicity) then 1 else 0 end +
case when profile.religion = any(pref.religion) then 1 else 0 end +
case when profile.zodiac_sign = any(pref.zodiac_sign) then 1 else 0 end +
case when profile.education = any(pref.education) then 1 else 0 end +
case when profile.hobby = any(pref.hobby) then 1 else 0 end +
case when (case when profile.is_green_head = pref.is_green_head is null then true else profile.is_green_head = pref.is_green_head end) then 1 else 0 end +
case when profile.preferred_relationship = pref.preferred_relationship then 1 else 0 end +
case when (select count(*) from liked_profile where liked_id = profile.id) >= pref.liked_count[1] and (select count(*) from liked_profile where liked_id = profile.id) <= pref.liked_count[2] then 1 else 0 end +
case when (select count(*) from liked_profile where liked_id = '{id}' and id = profile.id) > 0 then (select (random() * 2 + 2)::INT) else 0 end
) as point
FROM profile, pref, my_profile, strict where
{profile_filter}
{last_data}
and case when strict.gender = true then profile.gender = any(pref.gender) else true end
and case when strict.distance = true then calculate_distance(profile.latitude, profile.longitude, my_profile.latitude, my_profile.longitude, 'K') <= pref.distance else true end
and case when strict.age = true then (SELECT date_part('year', age(profile.birthday::timestamp))::int) >= pref.age[1] and (SELECT date_part('year', age(profile.birthday::timestamp))::int) <= pref.age[2] else true end
and case when strict.ethnicity = true then profile.ethnicity = any(pref.ethnicity) else true end
and case when strict.religion = true then profile.religion = any(pref.religion) else true end
and case when strict.zodiac_sign = true then profile.zodiac_sign = any(pref.zodiac_sign) else true end
and case when strict.education = true then profile.education = any(pref.education) else true end
and case when strict.hobby = true then profile.hobby = any(pref.hobby) else true end
and case when strict.is_green_head = true then (case when profile.is_green_head = pref.is_green_head is null then true else profile.is_green_head = pref.is_green_head end) else true end
and case when strict.preferred_relationship = true then profile.preferred_relationship = pref.preferred_relationship else true end
and case when strict.liked_count = true then (select count(*) from liked_profile where liked_id = profile.id) >= pref.liked_count[1] and (select count(*) from liked_profile where liked_id = profile.id) <= pref.liked_count[2] else true end
order by point desc limit {get_limit}
) profile
"""
        if is_valid_token(token):
            cur.execute("select is_vip from profile where id = %s", (id,))
            is_vip = fetch_data_in_list()
            if len(is_vip) > 0:
                is_vip = is_vip[0]
            else:
                is_vip = False
            get_daily_refresh_count(id)
            today_refresh_count = fetch_data_in_list()
            if len(today_refresh_count) > 0:
                today_refresh_count = today_refresh_count[0]
            else:
                today_refresh_count = 0
            cur.execute("select target_ids, created_at from last_data where id = %s", (id,))
            last_data = fetch_data_in_list()
            if len(last_data) > 0:
                last_created_at = str(last_data[1])
                original_target_ids = last_data[0]
                now = datetime.datetime.utcnow()
                last_created_at = datetime.datetime.strptime(last_created_at, datetime_format)
                next_fetch_time = last_created_at + datetime.timedelta(hours=get_data_interval)
                if (is_refresh and is_vip and vip_refresh_daily_count >= today_refresh_count) or now > next_fetch_time:
                    if is_refresh and is_vip:
                        # Count refresh time
                        insert_refresh(id)
                    last_data_where_clause = "and not (profile.id = any(%s))"
                    need_update_last_data = True
                else:
                    insert_log(uniqueid, too_frequent_data_log)
                    last_data_where_clause = "and profile.id = any(%s)"
            else:
                need_insert_last_data = True
                # Need insert before searching because if no row in last_data the searching will always return nth
                cur.execute("insert into last_data (id, target_ids, created_at) values (%s, %s, {time})".format(time=get_time_now()), (id, [],))
                conn.commit()
            main_sql = main_sql.format(
                id=id,
                get_profile_body=get_profile_body(id),
                last_data=last_data_where_clause,
                get_limit=get_limit(id),
                profile_filter=get_profile_filter(id),
            )
            if not need_insert_last_data:
                cur.execute(main_sql, (get_profile_images_filter(), original_target_ids,))
            else:
                cur.execute(main_sql, (get_profile_images_filter(),))
            profiles = fetch_data_in_list_of_list()
            for profile in profiles:
                new_target_ids.append(profile[0])
            if need_update_last_data:
                cur.execute("update last_data set created_at = {time}, target_ids = %s where id = %s".format(time=get_time_now()), (new_target_ids, id,))
                conn.commit()
            if need_insert_last_data:
                # Update the last_date's target_ids to latest since we have inserted empty above
                cur.execute("update last_data set target_ids = %s where id = %s", (new_target_ids, id,))
                conn.commit()
            cur.execute("""
select {get_preference_body}
from preference where id = any(%s)
            """.format(get_preference_body=get_preference_body()), (new_target_ids,))
            preferences = fetch_data_in_list_of_list()
            insert_log(uniqueid, get_potential_log, id)
            return jsonify({DataKey.status: Status.ok, DataKey.profiles: profiles, DataKey.preferences: preferences, DataKey.remaining_count: vip_refresh_daily_count - today_refresh_count - 1})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/viewProfileLog'.format(endpoint=endpoint), methods=['POST'])
def view_profile_api():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        target_id = request.form[DataKey.target_id]
        if is_valid_token(token):
            cur.execute("insert into viewed_profile (id, viewed_id, created_at) values (%s, %s, {time})".format(time=get_time_now()), (id, target_id,))
            conn.commit()
            insert_into_activity(id, target_id, Activity.view_profile)
            insert_log(uniqueid, view_profile_log + target_id, id=id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/reportUser'.format(endpoint=endpoint), methods=['POST'])
def report_user():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        target_id = request.form[DataKey.target_id]
        reason = request.form[DataKey.reason]
        if is_valid_token(token):
            new_report_id = str(uuid.uuid4())
            cur.execute("insert into report (report_id, id, target_id, reason, created_at) values (%s, %s, %s, %s, {time})".format(time=get_time_now()), (new_report_id, id, target_id, reason,))
            conn.commit()
            insert_log(uniqueid, report_user_log, id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/updateQuickDisplay'.format(endpoint=endpoint), methods=['POST'])
def update_quick_displays():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        quick_display_one = request.form[DataKey.quick_display_one]
        quick_display_two = request.form[DataKey.quick_display_two]
        if is_valid_token(token):
            cur.execute("update profile set quick_display_one = %s, quick_display_two = %s, last_active = {time}, updated_at = {time} where id = %s".format(time=get_time_now()), (quick_display_one, quick_display_two, id,))
            conn.commit()
            insert_log(uniqueid, update_quick_display_log, id)
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/getProfileById'.format(endpoint=endpoint), methods=['POST'])
def get_profile_by_id():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        target_id = request.form[DataKey.target_id]
        if is_valid_token(token):
            cur.execute("select {get_profile_body} from profile where id = %s".format(get_profile_body=get_profile_body(target_id)), (target_id,))
            profile = fetch_data_in_list_of_list()
            cur.execute("select {get_preference_body} from preference where id = %s".format(get_preference_body=get_preference_body()), (target_id,))
            preference = fetch_data_in_list_of_list()
            return jsonify({DataKey.status: Status.ok, DataKey.profile: profile, DataKey.preference: preference})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/readAllNotifications'.format(endpoint=endpoint), methods=['POST'])
def read_all_notifications():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        if is_valid_token(token):
            cur.execute("update activity set is_read = true where target_id = %s", (id,))
            conn.commit()
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/getPpById'.format(endpoint=endpoint), methods=['POST'])
def get_profile_and_preference_by_id():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        target_id = request.form[DataKey.target_id]
        if is_valid_token(token):
            cur.execute("""
select {get_profile_body}
from profile where id = %s
            """.format(get_profile_body=get_profile_body(id)), (target_id,))
            target_profile = fetch_data_in_list_of_list()
            cur.execute("""
select {get_preference_body}
from preference where id = %s
            """.format(get_preference_body=get_preference_body()), (target_id,))
            target_preference = fetch_data_in_list_of_list()
            insert_log(uniqueid, get_profile_log + target_id, id)
            return jsonify({DataKey.status: Status.ok, DataKey.profile: target_profile, DataKey.preference: target_preference})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/getMyProfileAndPreference'.format(endpoint=endpoint), methods=['POST'])
def get_my_profile_and_preference():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        email = request.form[DataKey.email]
        if is_valid_token(token):
            cur.execute("select id from profile where id = %s or email = %s", (id, email,))
            fetched_id = fetch_data_in_list()
            if len(fetched_id) > 0:
                fetched_id = fetched_id[0]
                cur.execute("select * from strict_preference where id = %s", (fetched_id,))
                my_strict_preference = fetch_data_in_list_of_list()
            cur.execute("""
select {get_profile_body}
from profile where id = %s or email = %s
            """.format(get_profile_body=get_profile_body(id)), (id, email,))
            my_profile = fetch_data_in_list_of_list()
            cur.execute("""
select {get_preference_body}
from preference where id = %s or email = %s
            """.format(get_preference_body=get_preference_body()), (id, email,))
            my_preference = fetch_data_in_list_of_list()
            insert_log(uniqueid, get_my_profile_log, id)
            return jsonify({DataKey.status: Status.ok, DataKey.my_profile: my_profile, DataKey.my_preference: my_preference, DataKey.cat_key: cat_key, DataKey.my_strict_preference: my_strict_preference})
        else:
            insert_log(uniqueid, wrong_token_log, id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/likeProfile'.format(endpoint=endpoint), methods=['POST'])
def like_unlike_profile():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        id = request.form[DataKey.id]
        target_id = request.form[DataKey.target_id]
        uniqueid = request.form[DataKey.uniqueid]
        is_discover = request.form[DataKey.is_discover] == true_text
        if is_valid_token(token):
            cur.execute("select id from passed_profile where id = %s and passed_id = %s union all select id from liked_profile where id = %s and liked_id = %s", (id, target_id, id, target_id,))
            is_passed_or_liked = fetch_data_in_list()
            if len(is_passed_or_liked) > 0:
                return jsonify({DataKey.status: Status.already_passed})
            if is_discover:
                cur.execute("select is_vip from profile where id = %s", (id,))
                is_vip = fetch_data_in_list()
                if len(is_vip) > 0:
                    is_vip = is_vip[0]
                else:
                    is_vip = False
                cur.execute("""
select count(*) from liked_profile where id = %s and is_discover is True and created_at between NOW() - INTERVAL '24 HOURS' AND NOW()
                """.format(discover_like=Activity.discover_like), (id,))
                today_discover_count = fetch_data_in_list()
                if len(today_discover_count) > 0:
                    today_discover_count = today_discover_count[0]
                else:
                    today_discover_count = 0
                if today_discover_count < non_vip_daily_discover_count or is_vip:
                    insert_into_activity(id, target_id, Activity.discover_like)
                    insert_log(uniqueid, like_profile_log, id=id)
                else:
                    insert_log(uniqueid, exceed_discover_like + target_id, id=id)
                    return jsonify({DataKey.status: Status.exceed_discover_allow})
            else:
                insert_into_activity(id, target_id, Activity.like_profile)
            cur.execute("insert into liked_profile (id, liked_id, is_discover, created_at) values (%s, %s, %s, {time})".format(
                time=get_time_now()), (id, target_id, is_discover,))
            conn.commit()
            cur.execute("select color, email, first_name, last_name, images from profile where id = %s", (id,))
            my_details = fetch_data_in_list()
            cur.execute("select color, email, first_name, last_name, images from profile where id = %s", (target_id,))
            target_details = fetch_data_in_list()
            send_push(target_id, PushTitle.liked, get_full_name(my_details[2], my_details[3]) + PushMessage.liked)
            # Check if target id has liked this id or not
            # if yes, create a chat room for both of them
            cur.execute("select id from liked_profile where id = %s and liked_id = %s", (target_id, id,))
            data = fetch_data_in_list()
            if len(data) > 0:
                if len(target_details) > 0 and len(my_details) > 0:
                    new_match_id = str(uuid.uuid4())
                    cur.execute("insert into matched (match_id, id, matched_id, created_at) values (%s, %s, %s, {time}), (%s, %s, %s, {time})".format(time=get_time_now()), (new_match_id, id, target_id, new_match_id, target_id, id,))
                    conn.commit()
                    last_message = "Start talking now!💞"
                    time_now = get_current_datetime()
                    # insert matched notification
                    insert_into_activity(id, target_id, Activity.matched)
                    insert_into_activity(target_id, id, Activity.matched)
                    # Send push notification
                    send_push(id, PushTitle.matched, get_full_name(target_details[2], target_details[3]) + PushMessage.matched)
                    send_push(target_id, PushTitle.matched, get_full_name(my_details[2], my_details[3]) + PushMessage.matched)
                    target_chat_docs = list(firestore_db.collection(chat_db_name).where(ChatTable.sender_id, u'==', id).where(ChatTable.target_id, u'==', target_id).get())
                    if len(target_chat_docs) == 0:
                        target_ref = firestore_db.collection(chat_db_name).add({
                            ChatTable.sender_id: id,
                            ChatTable.target_id: target_id,
                            ChatTable.updated_at: time_now,
                            ChatTable.created_at: time_now,
                            ChatTable.target_color: target_details[0],
                            ChatTable.target_email: target_details[1],
                            ChatTable.target_first_name: target_details[2],
                            ChatTable.target_last_name: target_details[3],
                            ChatTable.target_images: target_details[4],
                            ChatTable.last_message: last_message,
                            ChatTable.is_last_message_myself: False,
                            ChatTable.has_unread: True,
                         })
                    my_chat_docs = list(firestore_db.collection(chat_db_name).where(ChatTable.sender_id, u'==', target_id).where(ChatTable.target_id, u'==', id).get())
                    if len(my_chat_docs) == 0:
                        my_ref = firestore_db.collection(chat_db_name).add({
                            ChatTable.sender_id: target_id,
                            ChatTable.target_id: id,
                            ChatTable.updated_at: time_now,
                            ChatTable.created_at: time_now,
                            ChatTable.target_color: my_details[0],
                            ChatTable.target_email: my_details[1],
                            ChatTable.target_first_name: my_details[2],
                            ChatTable.target_last_name: my_details[3],
                            ChatTable.target_images: my_details[4],
                            ChatTable.last_message: last_message,
                            ChatTable.is_last_message_myself: False,
                            ChatTable.has_unread: True,
                        })
            return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(uniqueid, wrong_token_log, id=id)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/passProfile'.format(endpoint=endpoint), methods=['POST'])
def pass_profile():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        id = request.form[DataKey.id]
        target_id = request.form[DataKey.target_id]
        if is_valid_token(token):
            cur.execute("select id from passed_profile where id = %s and passed_id = %s union all select id from liked_profile where id = %s and liked_id = %s", (id, target_id, id, target_id,))
            data = fetch_data_in_list()
            if len(data) == 0:
                cur.execute("insert into passed_profile (id, passed_id, created_at) values (%s, %s, {time})".format(time=get_time_now()), (id, target_id,))
                conn.commit()
                insert_log(uniqueid, pass_profile_log)
                return jsonify({DataKey.status: Status.ok})
            else:
                return jsonify({DataKey.status: Status.already_liked})
        else:
            insert_log(uniqueid, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/deleteSecret'.format(endpoint=endpoint), methods=['POST'])
def remove_token():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        token = request.form[DataKey.token]
        uniqueid = request.form[DataKey.uniqueid]
        if is_valid_token(token):
            r.delete(token)
            insert_log(uniqueid, remove_token_log)
        else:
            insert_log(uniqueid, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: Status.exception_happened})


@main.route('{endpoint}/registerFCMToken'.format(endpoint=endpoint), methods=['POST'])
def register_fcm_token():
    try:
        if filter_blacklisted_ips_and_bots():
            return jsonify({DataKey.status: Status.blacklisted})
        id = request.form[DataKey.id]
        token = request.form[DataKey.token]
        platform = request.form[DataKey.platform]
        fcm_token = request.form[DataKey.fcm_token]
        if is_valid_token(token):
            # Check if this fcm token has already been registered
            cur.execute("select fcm_token from fcm_registration where fcm_token = %s", (fcm_token,))
            fcm_token_list = fetch_data_in_list()
            if len(fcm_token_list) > 0:
                return jsonify({DataKey.status: Status.duplicate_fcm_token})
            else:
                cur.execute("delete from fcm_registration where id = %s", (id,))
                conn.commit()
                cur.execute("insert into fcm_registration (id, fcm_token, device_platform, created_at) values (%s, %s, %s, {time})".format(
                        time=get_time_now()), (id, fcm_token, platform,))
                conn.commit()
                return jsonify({DataKey.status: Status.ok})
        else:
            insert_log(id, wrong_token_log)
            return jsonify({DataKey.status: Status.wrong_token})
    except Exception as e:
        print("Error")
        print("Caught exception: ", e)
        api_handle_cursor_closed(e)
        conn.rollback()
        return jsonify({DataKey.status: 405})


@main.route('/healthCheck', methods=['GET'])
def testing():
    print("Health check called")
    return "OK"
