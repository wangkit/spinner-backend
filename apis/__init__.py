from flask import Flask
from flask_talisman import Talisman
from utility.utils import *
from flask_limiter import Limiter
from flask_cors import CORS


def create_app(debug=False):
    """Create an application."""
    app = Flask(__name__)
    # limiter = Limiter(
    #     app,
    #     key_func=get_limiter_ip,
    #     default_limits=["20 per second, 100 per minute, 500 per hour, 10000 per day"]
    # )
    Talisman(app)
    app.debug = debug
    CORS(app)
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    print(app.url_map)
    return app






