import os
import tokenlib

version = "V1"
endpoint = "/spinner/dating/{version}".format(version=version)
valid_token = "Valid"
otp_key = "otp_key"
otp_expire_time = 30
vip_refresh_daily_count = 3
non_vip_daily_discover_count = 4
message_db_name = "message"
chat_db_name = "chat"
otp_time = "otp_time"
get_discover_interval = 24 # Hours
get_data_interval = 12 # Hours
email_time = "email_time"
true_text = 'true'
null_text = 'null'
ip_header = "HTTP_X_FORWARDED_FOR"
support_email = "Spinner <support@yarner.app>"
datetime_format = "%Y-%m-%d %H:%M:%S"
placeholder_link = "https://res.cloudinary.com/hyclslbe2/image/upload/v1588150703/profile/placeholder_s9bajp.png"
fcm_api_key = os.environ['FCM_TOKEN']
cat_key = os.environ['CAT_KEY']
restart_key = os.environ['RESTART_TOKEN']
mailgun_apikey = os.environ['EMAIL_KEY']
mailgun_domain = os.environ['EMAIL_DOMAIN']
auto_pass = os.environ['AUTO_PASS']
auto_otp = os.environ['AUTO_OTP']
cloud_vision_endpoint = os.environ['COMPUTER_VISION_ENDPOINT']
cloud_vision_key = os.environ['COMPUTER_VISION_SUBSCRIPTION_KEY']
analyze_image_url = cloud_vision_endpoint + "vision/v3.1/analyze"
heroku_api_key = os.environ['HEROKU_API_KEY']
heroku_app_name = os.environ['APP_NAME']
secret_key = os.environ['SECRET_KEY']
mailgun_url = mailgun_domain + "/messages"
manager = tokenlib.TokenManager(secret=secret_key, timeout=9999999999999999999 * 9999999999999999999)
auto_email_remark_footer = "\n\n**********[THIS IS AN AUTOMATICALLY GENERATED EMAIL. PLEASE DO NOT REPLY TO THIS EMAIL.]**********"
