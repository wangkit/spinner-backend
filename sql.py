from constant.const import get_data_interval, get_discover_interval, vip_refresh_daily_count, \
    non_vip_daily_discover_count
from values.activity import Activity


def get_profile_body(myId):
    return """
id,
first_name,
last_name,
email,
password,
country,
city,
longitude,
latitude,
gender,
color,
birthday,
(SELECT date_part('year', age(birthday::timestamp))::int),
ethnicity,
religion,
zodiac_sign,
profession,
education,
graduated_from,
personality,
preferred_relationship,
is_green_head,
hobby,
images,
verification_image,
verification_instruction,
video,
status,
{last_active},
{updated_at},
{created_at},
(select case count(*) when 0 then false else true end from liked_profile where liked_profile.id = '{myId}' and liked_profile.liked_id = profile.id) as is_liked,
(select count(*) from liked_profile where liked_id = profile.id) as liked_count,
(select count(*) > 0 from verified_profile where id = '{myId}' and verified_id = profile.id) as is_verified,
(select count(*) > 0 from robot_profile where id = '{myId}' and robot_id = profile.id) as is_robot,
(select {get_data_interval} from last_data where id = '{myId}'),
(select {get_discover_interval} from last_discover where id = '{myId}'),
(select 
count(activity_id)
from activity
where 
target_id = '{myId}'
and id != '{myId}'
and is_read = false),
is_vip,
is_push,
(select {daily_count} - count(*) from refresh where id = '{myId}' and created_at between NOW() - INTERVAL '24 HOURS' AND NOW()),
video_thumbnail,
(select count(*) > 0 from liked_profile where liked_profile.id = profile.id and liked_id = '{myId}'),
quick_display_one,
quick_display_two,
(select count(*) > 0 from greeting where id = '{myId}' and target_id = profile.id),
(select {non_vip_daily_discover_count} - count(*) from liked_profile where id = '{myId}' and is_discover is True and created_at between NOW() - INTERVAL '24 HOURS' AND NOW()),
(select count(*) > 0 from blocked where id = profile.id and target_id = '{myId}'),
(select count(*) from matched where id = profile.id),
(select count(*) from greeting where id = profile.id),
(select {created_at} from liked_profile where id = profile.id order by created_at desc limit 1),
(select (select count(*) > 0 from liked_profile where liked_id = profile.id and liked_profile.id = '{myId}') or (select count(*) > 0 from passed_profile where passed_id = profile.id and passed_profile.id = '{myId}'))
""".format(discover_like=Activity.discover_like,
           non_vip_daily_discover_count=non_vip_daily_discover_count,
           daily_count=vip_refresh_daily_count, myId=myId, last_active=get_to_char_timestamp('last_active'), updated_at=get_to_char_timestamp('updated_at'),
           created_at=get_to_char_timestamp('created_at'),
           get_data_interval=get_to_char_timestamp("last_data.created_at + interval '{get_data_interval} HOURS'".format(get_data_interval=get_data_interval)),
           get_discover_interval=get_to_char_timestamp("last_discover.created_at + interval '{get_discover_interval} HOURS'".format(get_discover_interval=get_discover_interval))
           )


def get_to_char_timestamp(value):
    return "COALESCE ( to_char ( {value}, 'YYYY-MM-DD HH24:MI:SS' ), '' )".format(value=value)


def get_preference_body():
    return """
gender,
distance,
age,
ethnicity,
religion,
zodiac_sign,
education,
hobby,
is_green_head,
preferred_relationship,
{updated_at},
liked_count,
id
""".format(updated_at=get_to_char_timestamp('updated_at'))
