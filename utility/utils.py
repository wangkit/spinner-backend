import ast
import itertools
import json
import uuid
import geoip2.database
import requests
from geopy import Nominatim
from pyfcm import FCMNotification
from tokenlib.errors import ExpiredTokenError, InvalidSignatureError, MalformedTokenError
from constant.const import *
import datetime, os
import psycopg2 as ps
import redis
from flask import request as flask_request, request

from status import TokenStatus, UserStatus
from values.log import adult_log

"""
Init firebase push notification with api key
"""
push_service = FCMNotification(api_key=fcm_api_key)

"""
Connect to postgresql
Creator connection and cursor
"""
DATABASE_URL = os.environ['DATABASE_URL']
conn = ps.connect(DATABASE_URL)
cur = conn.cursor()

"""
Connect to redis
"""
r = redis.from_url(os.environ.get("REDIS_URL"))


def fetch_data_in_list():
    fetched_data = cur.fetchall()
    return list(itertools.chain.from_iterable(fetched_data))


def fetch_data_in_list_of_list():
    fetched_data = cur.fetchall()
    return convert_to_list_of_list(fetched_data)


def insert_log(uniqueid, action, id=None):
    ip = get_ip()
    user_agent = get_user_agent()
    city = get_city_name(ip)
    country = get_country_name(ip)
    cur.execute("insert into log (uniqueid, id, action, ip, ip_city, ip_country, user_agent, created_at) values (%s, %s, %s, %s, %s, %s, %s, {time})".format(time=get_time_now()), (uniqueid, id, action, ip, city, country, user_agent,))
    conn.commit()


def get_daily_refresh_count(id):
    cur.execute("select count(*) from refresh where id = %s and created_at between NOW() - INTERVAL '24 HOURS' AND NOW()", (id,))


def insert_refresh(id):
    cur.execute("insert into refresh (id, created_at) values (%s, {time})".format(time=get_time_now()), (id,))
    conn.commit()


def send_email(from_email, to_email, subject, content):
    """
    :param from_email: The support email of Spinner of Yarns
    :param to_email: Target email address
    :param subject: Subject of the email
    :param content: Content of the email
    :return:
    """
    print("Send to: " + to_email)
    print("Subject: " + subject)
    r = requests.post(
        mailgun_url,
        auth=("api", mailgun_apikey),
        data={
            "from": from_email,
            "to": to_email,
            "subject": subject,
            "text": content,
        }
    )
    print("Send email reason: " + str(r.reason))
    print("Send email status: " + str(r.status_code))
    print("Send email text: " + str(r.text))


def get_ip():
    """
    :param request: The flask request received
    :return: The actual ip of the request hidden in ip_header
    """
    return request.environ.get(ip_header, request.remote_addr)


def get_limiter_ip():
    """
    :return: the ip address for the current request (or 127.0.0.1 if none found)
    """
    return flask_request.environ.get(ip_header, flask_request.remote_addr) or '127.0.0.1'


def blacklist_ip(uniqueid, ip, reason):
    cur.execute("insert into blacklisted_user (hint, ip, reason, created_at) values (%s, %s, %s, {time})".format(time=get_time_now()), (uniqueid, ip, reason))
    conn.commit()


def filter_blacklisted_ips_and_bots():
    ip = get_ip()
    print("Checking ip: " + str(ip))
    user_agent = get_user_agent()
    cur.execute("select blacklisted_user.ip from blacklisted_user where ip = %s", (ip,))
    blacklisted_list = cur.fetchall()
    blacklisted_list = list(itertools.chain.from_iterable(blacklisted_list))
    print("Checking blacklisted or not: " + str(blacklisted_list))
    print("Checking user agent: " + str(user_agent))
    if len(blacklisted_list) > 0 or "bot" in user_agent.lower() or "spider" in user_agent.lower():
        return True
    else:
        return False


def get_user_agent():
    return str(request.headers.get('User-Agent'))


def is_valid_uuid(value):
    try:
        uuid.UUID(str(value))
        return True
    except ValueError:
        return False


def has_digit(s):
    return any(i.isdigit() for i in s)


def has_alphabet(s):
    return any(i.isalpha() for i in s)


def get_fcm_token_by_id(id):
    """
    :param id: The id of the user
    :return: The fcm token stored in fcm_registration table retrieved from firebase
    """
    cur.execute(
        "select fcm_token from fcm_registration where id = %s", (id,))
    fcm_token = cur.fetchall()
    fcm_token = list(itertools.chain.from_iterable(fcm_token))
    if len(fcm_token) == 1:
        return fcm_token[0]
    else:
        return ""


def remove_whitespace(value):
    """
    :param value: Corner name or username
    :return: A name without whitespace
    """
    return value.replace(" ", "")


def convert_to_list_of_list(fetched_data):
    """
    :param fetched_data: Fetched list of tuple from sql cursor
    :return: A list of list for easier handling of data
    """
    return [list(ele) for ele in fetched_data]


def is_valid_token(token):
    token_status = get_token_status(token)
    return token_status == TokenStatus.ok


def convert_byte_to_str(value):
    return str(value).replace("b'", "").replace("'", "")


def get_token_status(token):
    """
    :param token: Token received from frontend
    :return: A boolean determining if the token is valid
    """
    try:
        parsed_token = manager.parse_token(token=token)
        result_from_r = r.get(token)
        result_from_r = convert_byte_to_str(result_from_r)
        if valid_token == result_from_r:
            return TokenStatus.ok
        else:
            return TokenStatus.wrong
    except ExpiredTokenError as e:
        print("expired")
        return TokenStatus.expired
    except InvalidSignatureError as e:
        print("invalid token")
        return TokenStatus.wrong
    except MalformedTokenError as e:
        print("malformed token")
        return TokenStatus.wrong


def assign_new_token(uniqueid):
    """
    :return: Create and set a new token
    """
    token = manager.make_token({'uniqueid': uniqueid})
    r.set(token, valid_token)
    return str(token)


def get_full_name(first_name, last_name):
    return first_name + " " + last_name


def send_push(id, title, message):
    cur.execute("select is_push from profile where id = %s", (id,))
    is_push = fetch_data_in_list()
    if len(is_push) > 0:
        is_push = is_push[0]
        if is_push:
            cur.execute("select fcm_token from fcm_registration where id = %s", (id,))
            fcm_token = fetch_data_in_list()
            if len(fcm_token) > 0:
                fcm_token = fcm_token[0]
                try:
                    result = push_service.notify_single_device(
                        registration_id=fcm_token,
                        message_title=title,
                        message_body=message
                    )
                    print("Push result: ", str(result))
                except Exception as e:
                    print("Push exception: " + str(e))


def get_longitude_and_latitude():
    geolocator = Nominatim(user_agent="my_user_agent")
    position = geolocator.geocode(get_city_name(get_ip()) + "," + get_country_name(get_ip()))
    return position


def check_is_adult(image_link, uniqueid):
    headers = {'Ocp-Apim-Subscription-Key': cloud_vision_key}
    params = {'visualFeatures': 'adult'}
    data = {'url': image_link}
    response = requests.post(analyze_image_url, headers=headers, params=params, json=data)
    analysis = response.json()
    print(json.dumps(response.json()))
    details = analysis["adult"]
    adult_score = details["adultScore"]
    racy_score = details["racyScore"]
    gore_score = details["goreScore"]
    is_adult = details["isAdultContent"]
    is_gory = details["isGoryContent"]
    is_racy = details["isRacyContent"]
    total_score = adult_score + racy_score + gore_score
    if is_adult or is_racy or is_gory or total_score / 3 > 1.7:
        insert_log(uniqueid, adult_log + str(total_score))
        return True
    else:
        return False


def get_profile_images_filter():
    return [placeholder_link for i in range(6)]


def get_profile_filter(id):
    return """
profile.id not in (
select robot_id from robot_profile where id = '{id}'
union all
select liked_id from liked_profile where id = '{id}'
union all
select passed_id from passed_profile where id = '{id}'
union all
select target_id from blocked where id = '{id}'
union all 
select blocked.id from blocked where blocked.target_id = '{id}' and blocked.id = profile.id
)
and profile.images != %s
and profile.id != '{id}' 
and profile.status = {normal_user}
    """.format(normal_user=UserStatus.normal, id=id)


def get_limit(id):
    cur.execute("select is_vip from profile where id = %s", (id,))
    is_vip = fetch_data_in_list()
    if len(is_vip) > 0:
        is_vip = is_vip[0]
        if is_vip:
            return "40"
    return "20"


def get_time_now():
    """
    :return: A part of sql with an accurate system time
    """
    time_now = str(datetime.datetime.utcnow().strftime(datetime_format))
    return "to_timestamp('{time_now}', 'yyyy-mm-dd hh24:mi:ss')".format(time_now=time_now)


def get_current_datetime():
    return datetime.datetime.strptime(datetime.datetime.utcnow().strftime(datetime_format), datetime_format)


def convert_str_to_list(value):
    return ast.literal_eval(value)


def insert_into_activity(id, target_id, activity, activity_id=None):
    if id != target_id:
        if activity_id is None:
            new_notification_id = str(uuid.uuid4())
        else:
            new_notification_id = activity_id
        cur.execute("update profile set last_active = {time} where id = %s".format(time=get_time_now()), (id,))
        conn.commit()
        cur.execute("insert into activity (activity_id, id, activity, target_id, created_at) values (%s, %s, %s, %s, {time})".format(time=get_time_now()), (new_notification_id, id, activity, target_id,))
        conn.commit()


def convert_str_to_list_of_int(value):
    return [int(i) for i in convert_str_to_list(value)]


def get_country_name(ip):
    """
    :param ip: Ip received
    :return: The name of the country of this ip
    """
    try:
        cwd = os.getcwd()
        country_reader = geoip2.database.Reader(cwd + "/country/GeoLite2-Country.mmdb")
        response = country_reader.country(ip)
        country_name = response.country.name
        if country_name is None or not country_name:
            country_name = "Unknown"
        # We have had issues that the country name contains a single quote or double quote, causing a failure of sql execution
        return country_name.replace("'", "")
    except Exception as e:
        print("Address not found: " + str(e))
        return "Unknown"


def get_city_name(ip):
    """
    :param ip: Ip received
    :return: The name of the city of this ip
    """
    try:
        cwd = os.getcwd()
        city_reader = geoip2.database.Reader(cwd + "/city/GeoLite2-City.mmdb")
        response = city_reader.city(ip)
        city_name = response.city.name
        if city_name is None or not city_name:
            city_name = "Unknown"
        # We have had issues that the city name contains a single quote or double quote, causing a failure of sql execution
        return city_name.replace("'", "")
    except Exception as e:
        print("Address not found: " + str(e))
        return "Unknown"


def api_handle_cursor_closed(error):
    if 'cursor already closed' in str(error) or 'terminating connection due to administrator command' in str(error):
        restart_dyno()


def restart_dyno():
    headers = {'Content-Type': 'application/json', 'Accept': 'application/vnd.heroku+json; version=3',
               'Authorization': 'Bearer ' + heroku_api_key}
    restart_dyno_request = requests.delete(
        "https://api.heroku.com/apps/" + heroku_app_name + "/dynos",
        headers=headers,
    )